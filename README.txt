Provides <a href="http://drupal.org/project/context">Context</a> support for <a href="http://drupal.org/project/blockcache_alter">Blockcache alter</a>. Based on a patch by trimsyndicate in [#1062796].

It currenty support both Context and Context Layouts block integration.

