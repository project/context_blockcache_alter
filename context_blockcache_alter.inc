<?php

/**
 * Implementation of *_reaction_block->build_block
 */
function _context_blockcache_alter_build_block($block, $reset = FALSE) {
  // Change, 1 line.
  // We
  // preserve the submission of forms in blocks, by fetching from cache
  // only if the request method is 'GET'.
  static $cacheable;
  if (!isset($cacheable) || $reset) {
    // Change, 1 line.
    $cacheable = $_SERVER['REQUEST_METHOD'] == 'GET';
  }
  if (!isset($block->content)) {
    $block->content = '';
    // Try fetching the block from cache.
    // Change, 3 lines including comment.
    // Can't use _block_get_cache_id() because context doesn't fetch block cache value from db.
    if ($cacheable && ($cid = _blockcache_alter_get_cache_id($block->module, $block->delta))) {
      if (($cache = cache_get($cid, 'cache_block')) && (_blockcache_alter_check_expire($cache, time()))) {
        $array = $cache->data;
      }
      else {
        // Change, 1 line.
        $array = module_invoke('blockcache_alter', 'block', 'view', $block->module . ',' . $block->delta);
        cache_set($cid, $array, 'cache_block', CACHE_TEMPORARY);
      }
    }
    // Otherwise build block.
    else {
      // Change, 1 line.
      $array = module_invoke('blockcache_alter', 'block', 'view', $block->module . ',' . $block->delta);
    }
    if (isset($array) && is_array($array)) {
      foreach ($array as $k => $v) {
        $block->$k = $v;
      }
    }
  }
  if (!empty($block->content)) {
    // Only query for custom block title if block core compatibility is enabled.
    if (!variable_get('context_reaction_block_disable_core', FALSE)) {
      global $user, $theme_key;
      static $block_titles = array();
      if (!isset($block_titles[$block->module][$block->delta])) {
        $result = db_query("SELECT module, delta, title FROM {blocks} WHERE theme = '%s'", $theme_key);
        while ($row = db_fetch_object($result)) {
          $block_titles[$row->module][$row->delta] = $row->title;
        }
      }
      $block->title = $block_titles[$module][$delta];
    }
    // Override default block title if a custom display title is present.
    if (!empty($block->title)) {
      // Check plain here to allow module generated titles to keep any markup.
      $block->subject = $block->title == '<none>' ? '' : check_plain($block->title);
    }
    if (!isset($block->subject)) {
      $block->subject = '';
    }
  }
  return $block;
}

