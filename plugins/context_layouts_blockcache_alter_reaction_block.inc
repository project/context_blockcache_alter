<?php

class context_layouts_blockcache_alter_reaction_block extends context_layouts_reaction_block {

  /**
   * Override of build_block() to add blockcache_alter functionality.
   */
  protected function build_block($block, $reset = FALSE) {
    module_load_include('inc', 'context_blockcache_alter', 'context_blockcache_alter');
    return _context_blockcache_alter_build_block($block, $reset);
  }
}

